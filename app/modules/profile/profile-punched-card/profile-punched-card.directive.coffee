###
# Copyright (C) 2018 Steeve Chailloux <steeve.chailloux@orus.io>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# File: modules/profile/profile-punched-card/profile-punched-card.directive.coffee
###

taiga = @.taiga
bindMethods = @.taiga.bindMethods

class ProfilePunchedCardController extends taiga.Controller
    @.$inject = [
        "tgUserService",
        "tgCurrentUserService"
        "$tgResources"
        "tgProjectLogoService"
        "$log"
    ]

    constructor: (@userService, @currentUserService, @rs, @logoService, @log) ->
        bindMethods(@)
        @.currentUser = @currentUserService.getUser()
        @.details = []

        @.isCurrentUser = false

        if @.currentUser && @.currentUser.get("id") == @.user.get("id")
            @.isCurrentUser = true

        @.date = new Date()

        @.getPunchedCard()

    getDaysInMonth: (year, month) ->
        month -= 1
        date = new Date(year, month, 1)
        while date.getMonth() == month
            prev = new Date(date.toString())
            date.setDate(date.getDate() + 1)
            prev

    isSameDate: (a, b) ->
        return a.toISOString().slice(0, 10) == b

    getPunchedCard: () =>
        @.details = []
        @.detailsDate = ""

        month = (@.date.getMonth() + 1).toString()
        month = "0#{month}" if month.length == 1
        year = @.date.getFullYear().toString()

        @rs.timeTracking["profile_punched_card"](@.user.get("id"), year, month)
            .then (res) =>
                days_stats = res.days_stats
                @.project_stats = res.project_stats
                @.us_stats = res.us_stats
                @.max = Math.max (v for k, v of days_stats)...
                @.setPunchedCard(days_stats, parseInt(year), parseInt(month))

    formatDate: (date) ->
        day = date.getDate().toString()
        day = "0#{day}" if day.length == 1
        month = (date.getMonth() + 1).toString()
        month = "0#{month}" if month.length == 1
        "#{date.getFullYear()}-#{month}-#{day}"

    setPunchedCard: (data, year, month) =>
        @.punchedCard = []

        daysInMonth = @.getDaysInMonth(year, month)
        nextDay = daysInMonth.shift()

        i = 0
        isEnded = false
        isFirst = true

        while not isEnded
            @.punchedCard[i] = []
            for day in [1,2,3,4,5,6,0]
                if day == nextDay.getDay()
                    val = data[@.formatDate(nextDay)]
                    if val?
                        @.punchedCard[i].push({
                            day: nextDay.getDate()
                            pts: val
                            bgfactor: Math.round(val * 100 / @.max) / 100
                        })
                    else
                        @.punchedCard[i].push({day: nextDay.getDate(), pts: 0, bgfactor: 0})
                    if daysInMonth.length > 0
                        nextDay = daysInMonth.shift()
                else
                    @.punchedCard[i].push({day: false, pts: 0, bgfactor: 0})


            if not isFirst and daysInMonth.length == 0
                isEnded = true

            i += 1
            isFirst = false

    getDayEntries: (day) =>
        @log.debug("Get day's entries for day", day)
        @.detailsDate = ""

        if !day
            @.details = []
            return

        day = "0#{day}" if day.toString().length == 1
        month = (@.date.getMonth() + 1).toString()
        month = "0#{month}" if month.length == 1
        year = @.date.getFullYear().toString()

        @.detailsDate = "#{day}/#{month}/#{year}"

        @rs.timeTracking["profile_punched_card_day"](@.user.get("id"), year, month, day)
            .then (res) =>
                @.details = res

    getProjectLogo: (detail) =>
        @log.debug(detail.logo)
        if detail.logo
            {src: detail.logo, color: "initial"}
        else
            @logoService.getDefaultProjectLogo(detail.slug, detail.project_id)

angular.module("taigaProfile")
    .controller("ProfilePunchedCard", ProfilePunchedCardController)

ProfilePunchedCardDirective = () ->
    link = (scope, elm, attrs, ctrl) ->
        null

    return {
        templateUrl: "profile/profile-punched-card/profile-punched-card.html",
        scope: {
            user: "="
        },
        controllerAs: "vm",
        controller: "ProfilePunchedCard",
        link: link,
        bindToController: true
    }

angular.module("taigaProfile").directive("tgProfilePunchedCard", ProfilePunchedCardDirective)
