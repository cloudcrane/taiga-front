###
# Copyright (C) 2018 Steeve Chailloux <steeve.chailloux@orus.io>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# File: modules/common/time-trackings.coffee
###

taiga = @.taiga
bindMethods = @.taiga.bindMethods
bindOnce = @.taiga.bindOnce
debounce = @.taiga.debounce
generateHash = taiga.generateHash

module = angular.module("taigaCommon")

class TimeTrackingsController extends taiga.Controller
    @.$inject = ["$scope", "$rootScope", "$tgRepo", "$tgResources", "$tgConfirm", "$q"]

    constructor: (@scope, @rootscope, @repo, @rs, @confirm, @q) ->
        bindMethods(@)
        @.timeTrackings = []
        @.showEstimate = false

    initialize: (type, objectId) ->
        @.project = @scope.project
        @.type = type
        @.objectId = objectId
        @.projectId = @scope.projectId
        @.has_perm = @.project.my_permissions.indexOf("view_time_tracking") != -1
        @.api = @rs.timeTracking[@.type]
        @.showEstimate = @.type != 'task'

        @scope.$on("time-tracking:save", @.createOrUpdate)
        @scope.$on("time-tracking:create", @.create)
        @scope.$on("time-tracking:update", @.update)
        @scope.$on("time-tracking:delete", @.delete)
        @scope.$on "time-tracking:add-new-clicked", =>
            @scope.$broadcast("time-tracking:show-form")

        if @.type == "userstory"
            self = @
            @rs.timeTracking[@.type + "_total"](@.objectId).then (res) ->
                self.total = () -> res

    createOrUpdate: (ctx, timeTracking) =>
        if timeTracking.pk?
            @update(null, timeTracking)
        else
            @.api.post(@.objectId, timeTracking).then (() =>
                @rootscope.$broadcast("object:updated")  # in order to update history
                @loadTimeTrackings()
                @scope.$broadcast("time-tracking:saved")), (error) =>
                    @scope.$broadcast("time-tracking:error", timeTracking.pk)
                    @confirm.notify("error", value, key) for key, value of error.data

    create: (ctx, timeTracking, hash) =>
        @.api.post(@.objectId, timeTracking).then (() =>
            @rootscope.$broadcast("object:updated")  # in order to update history
            @loadTimeTrackings()
            @scope.$broadcast("time-tracking:saved", hash)), (error) =>
                @scope.$broadcast("time-tracking:error", hash)
                @confirm.notify("error", value, key) for key, value of error.data

    update: (ctx, timeTracking) =>
        @.api.put(@.objectId, timeTracking.pk, timeTracking).then (() =>
            @rootscope.$broadcast("object:updated")  # in order to update history
            @scope.$broadcast("time-tracking:updated", timeTracking.pk)), (error) =>
                @scope.$broadcast("time-tracking:error", timeTracking.pk)
                @confirm.notify("error", value, key) for key, value of error.data

    delete: (ctx, id) =>
        @.api.delete(@.objectId, id).then (() =>
            @rootscope.$broadcast("object:updated")  # in order to update history
            @loadTimeTrackings()
            @scope.$broadcast("time-tracking:deleted", id, false)), (error) =>
                @scope.$broadcast("time-tracking:deleted", id, true)
                @confirm.notify("error", value, key) for key, value of error.data

    loadTimeTrackings: =>
        return @.timeTrackings if not @.objectId or @.type == "userstory"
        return @.api.list(@.objectId).then (timeTrackings) =>
            @.timeTrackings = timeTrackings
            return timeTrackings

    total: =>
        res = _.reduce((tt.pts for tt in @.timeTrackings), (acc, num) -> acc + num)
        return res


module.component(
    "tgTimeTrackingTotal",
    {
        template: '<div ng-show="$ctrl.total() >= 0">' +
            '{{ \'COMMON.TIME_TRACKING.TOTAL\' | translate }}: ' +
            '{{ $ctrl.total() }} Pts.</div>',
        bindings: { total: "=" }
    }
)

class TimeTrackingsEstimateController extends taiga.Controller
    @.$inject = ["$scope", "$rootScope", "$q", "$tgResources"]

    constructor: (@scope, @rootscope, @q, @rs) ->
        bindMethods(@)
        @.editMode = false
        @.estimate = 0
        @.api = @rs.timeTracking[@.type + "_estimate"]
        @.has_edit_perm = @.project.my_permissions.indexOf("modify_time_tracking") != -1
        @.api.list(@.objectId).then (res) =>
            if res[0]?
                @.estimateId = res[0].pk
                @.estimate = res[0].pts

    action: () =>
        @.save() if @.editMode
        @.editMode = !@.editMode

    save: () =>
        if @.estimateId?
            @.api.put(@.objectId, @.estimateId, { pts: @.estimate }).then () =>
                @rootscope.$broadcast("object:updated")  # in order to update history
        else
            @.api.post(@.objectId, { pts: @.estimate }).then (res) =>
                @rootscope.$broadcast("object:updated")  # in order to update history
                @.estimateId = res.pk


module.component(
    "tgTimeTrackingEstimate",
    {
        templateUrl: 'time-tracking/time-tracking-estimate.html'
        controller: TimeTrackingsEstimateController
        bindings: {
            type: "="
            objectId: "="
            project: "="
        }
    }
)

class TimeTrackingsTodoController extends taiga.Controller
    @.$inject = ["$scope", "$tgResources", "$translate"]

    constructor: (@scope, @rs, @translate) ->
        bindMethods(@)

        @.computableRoles = _.filter(@.project.roles, "computable")

        @rs.timeTracking[@.type + "_todo"](@.objectId).then (res) =>
            @.todos = res

    getRoleName: (slug) =>
        others = @translate.instant("COMMON.TIME_TRACKING.TODO_OTHERS")
        item = _.filter(@.computableRoles, (c) -> c.slug == slug)
        if item.length > 0 then item[0].name else others


module.component(
    "tgTimeTrackingTodo",
    {
        templateUrl: 'time-tracking/time-tracking-todo.html'
        controller: TimeTrackingsTodoController
        bindings: {
            type: "="
            objectId: "="
            project: "="
        }
    }
)


TimeTrackingsDirective = ($templates, $storage) ->
    template = $templates.get("time-tracking/time-trackings.html", true)

    collapsedHash = (type) ->
        return generateHash(["time-tracking-collapsed", type])

    link = ($scope, $el, $attrs, $ctrls) ->
        $ctrl = $ctrls[0]
        $model = $ctrls[1]
        hash = collapsedHash($attrs.type)
        $scope.collapsed = $storage.get(hash) or false

        bindOnce $scope, $attrs.ngModel, (value) ->
            $ctrl.initialize($attrs.type, value.id)
            $ctrl.loadTimeTrackings()

        $scope.toggleCollapse = () ->
            $scope.collapsed = !$scope.collapsed
            $storage.set(hash, $scope.collapsed)

        $scope.foobar = '123'

        $scope.$on "$destroy", ->
            $el.off()

    templateFn = ($el, $attrs) ->
        return template({
            requiredEditionPerm: $attrs.requiredEditionPerm,
            total: $attrs.total
        })

    return {
        require: ["tgTimeTrackings", "ngModel"]
        controller: TimeTrackingsController
        controllerAs: "ctrl"
        restrict: "AE"
        scope: true
        link: link
        template: templateFn
    }

module.directive(
    "tgTimeTrackings",
    [
        "$tgTemplate", "$tgStorage",
        TimeTrackingsDirective
    ])


TimeTrackingDirective = (
    $template,
    $selectedText,
    $compile,
    $translate,
    datePickerConfigService,
    wysiwygService,
    $loading,
    $confirm,
) ->
    templateView = $template.get("time-tracking/time-tracking.html", true)
    templateEdit = $template.get("time-tracking/time-tracking-edit.html", true)

    link = ($scope, $el, $attrs, $model) ->
        prettyDate = $translate.instant("COMMON.PICKERDATE.FORMAT")
        @childScope = $scope.$new()

        saveTimeTracking = debounce 2000, (timeTracking) ->
            if timeTracking.dateEdit
                timeTracking.date = moment(timeTracking.dateEdit).format("YYYY-MM-DD")

            $scope.hide = true
            selectors = ['.tt-loader div']
            currentLoaders = ($loading().target($el.find(selector)).start() for selector in selectors)

            $scope.$emit("time-tracking:save", timeTracking)
            $scope.$on "time-tracking:updated", (ctx, pk) ->
                if timeTracking.pk == pk
                    $scope.hide = false
                    currentLoader.finish() for currentLoader in currentLoaders
                    renderView($model.$modelValue)

            $scope.$on "time-tracking:error", (ctx, pk) ->
                if timeTracking.pk == pk
                    $scope.hide = false
                    currentLoader.finish() for currentLoader in currentLoaders

        renderEdit = (timeTracking) ->
            if timeTracking.date
                timeTracking.dateEdit = new Date(timeTracking.date)

            @childScope.$destroy()
            @childScope = $scope.$new()
            $el.off()
            childScope.showTodo = timeTracking.todo?
            $el.html($compile(templateEdit({
                timeTracking: timeTracking
            }))(childScope))

            $el.find(".tt-total-time input").val(timeTracking.pts)
            $el.find(".tt-total-time.todo input").val(timeTracking.todo)

            $el.on "keyup", "input", (event) ->
                if event.keyCode == 13
                    saveTimeTracking($model.$modelValue)
                else if event.keyCode == 27
                    renderView($model.$modelValue)

            $el.on "click", ".save-task", (event) ->
                saveTimeTracking($model.$modelValue)

            $el.on "click", ".cancel-edit", (event) ->
                renderView($model.$modelValue)

        renderView = (timeTracking) ->
            if timeTracking.date
                timeTracking.dateView = moment(timeTracking.date, "YYYY-MM-DD").format(prettyDate)

            perms = {
                modify_task: $scope.project.my_permissions.indexOf("modify_time_tracking") != -1
                delete_task: $scope.project.my_permissions.indexOf("delete_time_tracking") != -1
            }

            $scope.showTodo = timeTracking.todo?
            $el.html($compile(templateView({
                timeTracking: timeTracking
                perms: perms
                emojify: (text) -> $emojis.replaceEmojiNameByHtmlImgs(_.escape(text))
            }))($scope))

            $el.on "click", ".edit-time-tracking", ->
                renderEdit($model.$modelValue)
                $el.find('input').focus().select()

            $el.on "click", ".delete-time-tracking", (event) ->
                title = $translate.instant("COMMON.TIME_TRACKING.TITLE_DELETE_ACTION")
                timeTracking = $model.$modelValue
                message = timeTracking.subject

                $confirm.askOnDelete(title, message).then (askResponse) ->
                    $scope.$emit("time-tracking:delete", timeTracking.pk)

                    $scope.$on "time-tracking:deleted", (ctx, pk, onError) ->
                        if pk == timeTracking.pk
                            if onError
                                askResponse.finish(false)
                            else
                                askResponse.finish()

        $scope.$watch $attrs.ngModel, (val) ->
            return if not val
            renderView(val)

        $scope.$on "$destroy", ->
            $el.off()

    return {
        link: link
        require: "ngModel"
        restrict: "AE"
    }

module.directive(
    "tgTimeTracking",
    [
        "$tgTemplate", "$selectedText", "$compile", "$translate",
        "tgDatePickerConfigService", "tgWysiwygService", "$tgLoading",
        "$tgConfirm",
        TimeTrackingDirective
    ])


TimeTrackingCreateButtonDirective = ($repo, $compile, $confirm, $tgmodel, $template) ->
    template = $template.get("common/components/add-button.html", true)

    link = ($scope, $el, $attrs) ->
        $scope.$watch "project", (val) ->
            return if not val
            $el.off()
            if $scope.project.my_permissions.indexOf("add_time_tracking") != -1
                $el.html($compile(template())($scope))
            else
                $el.html("")

            $el.on "click", ".add-button", (event)->
                $scope.$emit("time-tracking:add-new-clicked")

        $scope.$on "$destroy", ->
            $el.off()

    return {link: link}

module.directive(
    "tgTimeTrackingCreateButton",
    [
        "$tgRepo", "$compile", "$tgConfirm", "$tgModel", "$tgTemplate",
        TimeTrackingCreateButtonDirective
    ])


TimeTrackingCreateFormDirective = ($repo, $compile, $confirm, $tgmodel, $loading, $analytics, $auth) ->
    collapsedHash = (timeTracking) ->
        return generateHash(["time-tracking", timeTracking, Date.now()])

    link = ($scope, $el, $attrs) ->
        newTimeTracking = {
            date: new Date()
            pts: null
            todo: null
            assigned_to: $auth.getUser().id
        }
        createTimeTracking = debounce 2000, (timeTracking, saveAndAdd) ->
            res = {}

            newTimeTracking = $scope.newTimeTracking

            res.pts = newTimeTracking.pts
            res.todo = newTimeTracking.todo
            res.assigned_to = newTimeTracking.assigned_to
            res.comment = newTimeTracking.comment
            if newTimeTracking.date
                res.date = moment(newTimeTracking.date).format("YYYY-MM-DD")
            hash = collapsedHash(res)

            $scope.newTimeTracking.hide = true
            selectors = ['.tt-loader div']
            currentLoaders = ($loading().target($el.find(selector)).start() for selector in selectors)

            $scope.$emit("time-tracking:create", res, hash)
            $scope.$on "time-tracking:saved", (ctx, _hash) ->
                if hash == _hash
                    $scope.newTimeTracking.hide = false
                    currentLoader.finish() for currentLoader in currentLoaders
                reset()
                if saveAndAdd
                    $el.find('input').focus()
                else
                    close()

            $scope.$on "time-tracking:error", (ctx, _hash) ->
                if hash == _hash
                    $scope.newTimeTracking.hide = false
                    currentLoader.finish() for currentLoader in currentLoaders
                    $el.find('input').focus()

        close = () ->
            $el.off()

            $scope.openNewTimeTracking = false

        reset = () ->
            newTimeTracking = {
                date: new Date()
                pts: null
                todo: null
                assigned_to: $auth.getUser().id
            }

            $scope.newTimeTracking = $tgmodel.make_model("timeTrackings", newTimeTracking)

        render = ->
            return if $scope.openNewTimeTracking

            $scope.openNewTimeTracking = true

            $el.on "keyup", "input", (event)->
                if event.keyCode == 13
                    createTimeTracking(newTimeTracking, true)

                else if event.keyCode == 27
                    $scope.$apply () ->
                        reset()
                        close()

        $scope.save = () ->
            createTimeTracking(newTimeTracking, false)

        taiga.bindOnce $scope, "project", reset

        $scope.$on "time-tracking:show-form", ->
            $scope.$apply(render)

        $scope.$on "$destroy", ->
            $el.off()

    return {
        scope: true,
        link: link,
        templateUrl: 'time-tracking/time-tracking-create-form.html'
    }

module.directive(
    "tgTimeTrackingCreateForm",
    [
        "$tgRepo", "$compile", "$tgConfirm", "$tgModel", "$tgLoading",
        "$tgAnalytics", "$tgAuth",
        TimeTrackingCreateFormDirective
    ])


TimeTrackingAssignedToInlineEditionDirective = ($repo, $rootscope, $translate, avatarService) ->
    template = _.template("""
    <img style="background-color: <%- bg %>" src="<%- imgurl %>" alt="<%- name %>"/>
    <figcaption><%- name %></figcaption>
    """)

    link = ($scope, $el, $attrs) ->
        updateTimeTracking = (timeTracking) ->
            ctx = {
                name: $translate.instant("COMMON.ASSIGNED_TO.NOT_ASSIGNED"),
            }

            member = $scope.usersById[timeTracking.assigned_to]

            avatar = avatarService.getAvatar(member)
            ctx.imgurl = avatar.url
            ctx.bg = avatar.bg

            if member
                ctx.name = member.full_name_display

            $el.find(".avatar").html(template(ctx))
            $el.find(".time-tracking-assignedto").attr('title', ctx.name)

        $ctrl = $el.controller()
        timeTracking = $scope.$eval($attrs.tgTimeTrackingAssignedToInlineEdition)
        notAutoSave = $scope.$eval($attrs.notAutoSave)
        autoSave = !notAutoSave

        $scope.$watch $attrs.tgTimeTrackingAssignedToInlineEdition, () ->
            timeTracking = $scope.$eval($attrs.tgTimeTrackingAssignedToInlineEdition)
            updateTimeTracking(timeTracking)

        updateTimeTracking(timeTracking)

        $el.on "click", ".time-tracking-assignedto", (event) ->
            $rootscope.$broadcast("assigned-to:add", timeTracking)

        taiga.bindOnce $scope, "project", (project) ->
            # If the user has not enough permissions the click events are unbinded
            if project.my_permissions.indexOf("modify_time_tracking") == -1
                $el.unbind("click")
                $el.find("a").addClass("not-clickable")

        $scope.$on "assigned-to:added", debounce 2000, (ctx, userId, updatedTimeTracking) ->
            if updatedTimeTracking.pk == timeTracking.pk
                updatedTimeTracking.assigned_to = userId
                if autoSave
                    $scope.$emit("time-tracking:update", timeTracking)
                    # $repo.save(updatedTimeTracking).then ->
                    #     $scope.$emit("related-time-trackings:assigned-to-changed")
                updateTimeTracking(updatedTimeTracking)

        $scope.$on "$destroy", ->
            $el.off()

    return {link: link}

module.directive(
    "tgTimeTrackingAssignedToInlineEdition",
    [
        "$tgRepo", "$rootScope", "$translate", "tgAvatarService",
        TimeTrackingAssignedToInlineEditionDirective
    ])

class TaskTypeController extends taiga.Controller
    @.$inject = ["$scope", "$rootScope", "$q", "$tgResources"]

    constructor: (@scope, @rootscope, @q, @rs) ->
        bindMethods(@)
        computableRoles = _.filter(@.project.roles, "computable")
        @.type = ''
        @.items = ([c.slug, c.name] for c in computableRoles)

        @.has_perm = @.project.my_permissions.indexOf("view_time_tracking") != -1

        @scope.$watch @.task, (task) =>
            @.load(task.id) if task?

    load: (taskId) =>
        @.taskId = taskId
        @rs.timeTracking.task_extentions.get(taskId).then (res) =>
            @.type = res.type

    update: =>
        params = {type: @.type}
        @rs.timeTracking.task_extentions.patch(@.taskId, params)


module.component(
    "tgTaskType",
    {
        templateUrl: 'time-tracking/task-type.html'
        controller: TaskTypeController
        bindings: {
            task: "&"
            project: "="
        }
    }
)
