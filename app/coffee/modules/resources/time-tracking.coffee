###
# Copyright (C) 2018 Steeve Chailloux <steeve.chailloux@orus.io>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# File: modules/common/time-tracking-values.coffee
###

taiga = @.taiga

ResourceProvider = (urls, http) ->

    _post = (url, urlExtra, objectId, params, prefix="time-tracking") ->
        url = urls.resolve("#{prefix}/#{url}#{urlExtra}", objectId)
        return http.post(url, params)
            .then (result) -> result.data

    _list = (url, urlExtra, objectId, prefix="time-tracking") ->
        url = urls.resolve("#{prefix}/#{url}#{urlExtra}", objectId)
        return http.get(url)
            .then (result) -> result.data

    _get = (url, urlExtra, objectId, id, prefix="time-tracking") ->
        url = urls.resolve("#{prefix}/#{url}#{urlExtra}", objectId) + "/#{id}"
        return http.get(url)
            .then (result) -> result.data

    _put = (url, urlExtra, objectId, id, params, prefix="time-tracking") ->
        url = urls.resolve("#{prefix}/#{url}#{urlExtra}", objectId) + "/#{id}"
        return http.put(url, params)
            .then (result) -> result.data

    _patch = (url, urlExtra, objectId, id, params, prefix="time-tracking") ->
        url = urls.resolve("#{prefix}/#{url}#{urlExtra}", objectId) + "/#{id}"
        return http.patch(url, params)
            .then (result) -> result.data

    _delete = (url, urlExtra, objectId, id, prefix="time-tracking") ->
        url = urls.resolve("#{prefix}/#{url}#{urlExtra}", objectId) + "/#{id}"
        return http.delete(url)
            .then (result) -> result.data

    service = {
        issue: {
            post: (objectId, params) -> _post("issue", "", objectId, params)
            list: (objectId) -> _list("issue", "", objectId)
            get: (objectId, id) -> _get("issue", "", objectId, id)
            put: (objectId, id, params) -> _put("issue", "", objectId, id, params)
            patch: (objectId, id, params) -> _patch("issue", "", objectId, id, params)
            delete: (objectId, id) -> _delete("issue", "", objectId, id)
        }
        task: {
            post: (objectId, params) -> _post("task", "", objectId, params)
            list: (objectId) -> _list("task", "", objectId)
            get: (objectId, id) -> _get("task", "", objectId, id)
            put: (objectId, id, params) -> _put("task", "", objectId, id, params)
            patch: (objectId, id, params) -> _patch("task", "", objectId, id, params)
            delete: (objectId, id) -> _delete("task", "", objectId, id)
        }
        task_extentions: {
            get: (objectId) ->
                url = urls.resolve("extentions/task", objectId)
                http.get(url)
                    .then (result) -> result.data

            patch: (objectId, params) ->
                url = urls.resolve("extentions/task", objectId)
                http.patch(url, params)
                    .then (result) -> result.data
        }
        issue_estimate: {
            post: (objectId, params) -> _post("issue", "-estimate", objectId, params)
            list: (objectId) -> _list("issue", "-estimate", objectId)
            get: (objectId, id) -> _get("issue", "-estimate", objectId, id)
            put: (objectId, id, params) -> _put("issue", "-estimate", objectId, id, params)
            patch: (objectId, id, params) -> _patch("issue", "-estimate", objectId, id, params)
            delete: (objectId, id) -> _delete("issue", "-estimate", objectId, id)
        }
        userstory_estimate: {
            post: (objectId, params) -> _post("userstory", "-estimate", objectId, params)
            list: (objectId) -> _list("userstory", "-estimate", objectId)
            get: (objectId, id) -> _get("userstory", "-estimate", objectId, id)
            put: (objectId, id, params) -> _put("userstory", "-estimate", objectId, id, params)
            patch: (objectId, id, params) -> _patch("userstory", "-estimate", objectId, id, params)
            delete: (objectId, id) -> _delete("userstory", "-estimate", objectId, id)
        }
        userstory_total: (objectId) -> _list("userstory", "-total", objectId)
        userstory_todo: (objectId) -> _list("userstory", "-todo", objectId)

        profile_punched_card: (userId, year, month) ->
            url = urls.resolve("time-tracking/punched-card", userId) + "/#{year}/#{month}"
            return http.get(url)
                .then (result) -> result.data

        profile_punched_card_day: (userId, year, month, day) ->
            url = urls.resolve("time-tracking/punched-card", userId) + "/#{year}/#{month}/#{day}"
            return http.get(url)
                .then (result) -> result.data
    }

    return (instance) ->
        instance.timeTracking = service

module = angular.module("taigaResources")
module.factory(
    "tgTimeTrackingResourceProvider",
    ["$tgUrls", "$tgHttp", ResourceProvider])
